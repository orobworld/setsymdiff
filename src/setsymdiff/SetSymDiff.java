package setsymdiff;

import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import org.apache.commons.collections4.CollectionUtils;

/**
 * SetSymmDiff compares multiple ways of computing the size of the symmetric
 * difference between two sets, with the sets implemented as either hash sets
 * (faster) or tree sets (more feature-rich).
 *
 * The sets for which the symmetric difference will be constructed will
 * hold integers between 0 and a specified upper limit. Command line arguments
 * control the details of the experiment.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class SetSymDiff {
  // Conversion factor from nanoseconds to microseconds.
  private static final double NANOTOMICRO = 0.001;
  // Default values for the command line arguments.
  private static final int NREPS = 10000;     // # of replications to perform
  private static final int MAXINT = 100000;   // upper limit of integer values
  private static final int MINSET = 100;      // minimum set size
  private static final int MAXSET = 200;      // maximum set size
  private static final int SEED = 51021;      // random number seed

  /**
   * Dummy constructor.
   */
  private SetSymDiff() { }

  /**
   * Runs the experiment.
   * There are five command line arguments (all integers), with default values
   * provided:
   *   (1) the number of replications to perform;
   *   (2) the upper limit to the range of integers (lower limit = 0);
   *   (3) the lower limit on set size (cardinality);
   *   (4) the upper limit on set size (cardinality); and
   *   (5) the seed for the random number generator.
   * The lower limit on set size is soft (may be violated in a few instances).
   * @param args the command line arguments
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {
    // Extract the parameters from the command line.
    int nReps = (args.length > 0) ? Integer.parseInt(args[0]) : NREPS;
    int maxInt = (args.length > 1) ? Integer.parseInt(args[1]) : MAXINT;
    int minSetSize = (args.length > 2) ? Integer.parseInt(args[2]) : MINSET;
    int maxSetSize = (args.length > 3) ? Integer.parseInt(args[3]) : MAXSET;
    int seed = (args.length > 4) ? Integer.parseInt(args[4]) : SEED;
    // Spawn a new instance of this class and run the experiment.
    (new SetSymDiff()).run(nReps, maxInt, minSetSize, maxSetSize, seed);
  }

  /**
   * Runs the computational experiment.
   * @param nReps the number of replications to perform
   * @param maxInt the upper limit to the range of integers
   * @param minSetSize the minimum cardinality of a set
   * @param maxSetSize the maximum cardinality of a set
   * @param seed the seed for the random number generator
   */
  private void run(final int nReps, final int maxInt, final int minSetSize,
                   final int maxSetSize, final int seed) {
    // Echo the arguments.
    System.out.println("Experimental parameters:"
                       + "\n\tnumber of replications = " + nReps
                       + "\n\tupper limit for integers = " + maxInt
                       + "\n\tminimum set cardinality = " + minSetSize
                       + "\n\tmaximum set cardinality = " + maxSetSize
                       + "\n\tseed value = " + seed);
    // Set up a random number generator.
    Random rng = new Random(seed);
    // Set up storage space for timing results for all methods.
    long[] stream = new long[nReps];
    long[] subtract = new long[nReps];
    long[] disjunct = new long[nReps];
    long[] characteristic = new long[nReps];
    long[] stream2 = new long[nReps];
    long[] intersect = new long[nReps];
    // Store the sizes of the set.
    int[] setSizes = new int[2 * nReps];
    // Test the various methods on hash sets.
    for (int rep = 0; rep < nReps; rep++) {
      // Create a pair of sets with random content and track their sizes.
      HashSet<Integer> s1 = new HashSet<>();
      Arrays.stream(getRandomInts(minSetSize, maxSetSize, maxInt, rng))
            .forEach(i -> s1.add(i));
      HashSet<Integer> s2 = new HashSet<>();
      Arrays.stream(getRandomInts(minSetSize, maxSetSize, maxInt, rng))
            .forEach(i -> s2.add(i));
      setSizes[rep] = s1.size();
      setSizes[rep + nReps] = s2.size();
      // Compute the symmetric difference size using streams.
      long start = System.nanoTime();
      long n1 = streamDiff(s1, s2);
      stream[rep] = System.nanoTime() - start;
      // Compute the symmetric difference using the Apache Commons Collections
      // subtract() method.
      start = System.nanoTime();
      long n2 = subDiff(s1, s2);
      subtract[rep] = System.nanoTime() - start;
      // Compute the symmetric difference using the Apache Commons Collections
      // disjunction method.
      start = System.nanoTime();
      long n3 = CollectionUtils.disjunction(s1, s2).size();
      disjunct[rep] = System.nanoTime() - start;
      // Compute the symmetric difference using the characteristic vector.
      start = System.nanoTime();
      long n4 = charDiff(s1, s2, maxInt);
      characteristic[rep] = System.nanoTime() - start;
      // Compute the symmetric difference using the intersection (via streams).
      start = System.nanoTime();
      long n5 = streamDiff2(s1, s2);
      stream2[rep] = System.nanoTime() - start;
      // Compute the symmetric difference using the intersection (via Apache).
      start = System.nanoTime();
      long n6 = intersect(s1, s2);
      intersect[rep] = System.nanoTime() - start;
      // Sanity check: make sure all methods produce the same result.
      List<Long> results = List.of(n1, n2, n3, n4, n5, n6);
      if (Collections.min(results) != Collections.max(results)) {
        throw new RuntimeException("Discrepancy occurred!");
      }
    }
    // Compute average times (microseconds) for each method.
    System.out.println("\nResults for hash sets (microseconds):");
    System.out.println("\tStreams: " + asMicroSeconds(stream));
    System.out.println("\tSubtract: " + asMicroSeconds(subtract));
    System.out.println("\tDisjunction: " + asMicroSeconds(disjunct));
    System.out.println("\tCharacteristic vector: "
                       + asMicroSeconds(characteristic));
    System.out.println("\tIntersection (streams): " + asMicroSeconds(stream2));
    System.out.println("\tIntersection (Collections): "
                       + asMicroSeconds(intersect));
    // Display the distribution of set sizes.
    System.out.println("Set size distribution:\n"
                       + Arrays.stream(setSizes).summaryStatistics());
    // Repeat using tree sets.
    for (int rep = 0; rep < nReps; rep++) {
      // Create a pair of sets with random content and track their sizes.
      TreeSet<Integer> s1 = new TreeSet<>();
      Arrays.stream(getRandomInts(minSetSize, maxSetSize, maxInt, rng))
            .forEach(i -> s1.add(i));
      TreeSet<Integer> s2 = new TreeSet<>();
      Arrays.stream(getRandomInts(minSetSize, maxSetSize, maxInt, rng))
            .forEach(i -> s2.add(i));
      setSizes[rep] = s1.size();
      setSizes[rep + nReps] = s2.size();
      // Compute the symmetric difference size using streams.
      long start = System.nanoTime();
      long n1 = streamDiff(s1, s2);
      stream[rep] = System.nanoTime() - start;
      // Compute the symmetric difference using the Apache Commons Collections
      // subtract() method.
      start = System.nanoTime();
      long n2 = subDiff(s1, s2);
      subtract[rep] = System.nanoTime() - start;
      // Compute the symmetric difference using the Apache Commons Collections
      // disjunction method.
      start = System.nanoTime();
      long n3 = CollectionUtils.disjunction(s1, s2).size();
      disjunct[rep] = System.nanoTime() - start;
      // Compute the symmetric difference using the characteristic vector.
      start = System.nanoTime();
      long n4 = charDiff(s1, s2, maxInt);
      characteristic[rep] = System.nanoTime() - start;
      // Compute the symmetric difference using the intersection (via streams).
      start = System.nanoTime();
      long n5 = streamDiff2(s1, s2);
      stream2[rep] = System.nanoTime() - start;
      // Compute the symmetric difference using the intersection (via Apache).
      start = System.nanoTime();
      long n6 = intersect(s1, s2);
      intersect[rep] = System.nanoTime() - start;
      // Sanity check: make sure all three produce the same result.
      List<Long> results = List.of(n1, n2, n3, n4, n5, n6);
      if (Collections.min(results) != Collections.max(results)) {
        throw new RuntimeException("Discrepancy occurred!");
      }
    }
    // Compute average times (microseconds) for each method.
    System.out.println("\nResults for tree sets (microseconds):");
    System.out.println("\tStreams: " + asMicroSeconds(stream));
    System.out.println("\tSubtract: " + asMicroSeconds(subtract));
    System.out.println("\tDisjunction: " + asMicroSeconds(disjunct));
    System.out.println("\tCharacteristic vector: "
                       + asMicroSeconds(characteristic));
    System.out.println("\tIntersection (streams): " + asMicroSeconds(stream2));
    System.out.println("\tIntersection (Collections): "
                       + asMicroSeconds(intersect));
    // Display the distribution of set sizes.
    System.out.println("Set size distribution:\n"
                       + Arrays.stream(setSizes).summaryStatistics());
  }

  /**
   * Computes the size of the symmetric difference of two sets using streams.
   * @param s1 the first set
   * @param s2 the second set
   * @return the cardinality of the symmetric difference
   */
  private long streamDiff(final Set<Integer> s1, final Set<Integer> s2) {
    long diff = s1.stream().filter(i -> !s2.contains(i)).count();
    diff += s2.stream().filter(i -> !s1.contains(i)).count();
    return diff;
  }

  /**
   * Computes the size of the symmetric difference of two sets using the
   * Apache Commons Collections subtract() method.
   * @param s1 the first set
   * @param s2 the second set
   * @return the cardinality of the symmetric difference
   */
  private long subDiff(final Set<Integer> s1, final Set<Integer> s2) {
    long diff = CollectionUtils.subtract(s1, s2).size();
    diff += CollectionUtils.subtract(s2, s1).size();
    return diff;
  }

  /**
   * Computes the size of the symmetric difference of two sets using the
   * characteristic vector of the difference.
   * @param s1 the first set
   * @param s2 the second set
   * @param max the largest possible integer
   * @return the cardinality of the symmetric difference
   */
  private long charDiff(final Set<Integer> s1, final Set<Integer> s2,
                        final int max) {
    BitSet bits = new BitSet(max + 1);
    s1.stream().forEach(i -> bits.set(i));
    s2.stream().forEach(i -> bits.flip(i));
    return bits.cardinality();
  }

  /**
   * Computes the size of the symmetric difference of two sets using streams.
   * In this version, streams are used to get the size of the intersection,
   * which in turn yields the size of the symmetric difference.
   * @param s1 the first set
   * @param s2 the second set
   * @return the cardinality of the symmetric difference
   */
  private long streamDiff2(final Set<Integer> s1, final Set<Integer> s2) {
    return s1.size() + s2.size()
           - 2 * s1.stream().filter(i -> s2.contains(i)).count();
  }


  /**
   * Computes the size of the symmetric difference of two sets using the
   * Apache Commons Collections intersection() method.
   * @param s1 the first set
   * @param s2 the second set
   * @return the cardinality of the symmetric difference
   */  private long intersect(final Set<Integer> s1, final Set<Integer> s2) {
    return s1.size() + s2.size()
           - 2 * CollectionUtils.intersection(s1, s2).size();
  }

  /**
   * Generates a random integer vector within a specified size range.
   * The contents will be between 0 (inclusive) and the specified upper limit
   * (exclusive).
   * @param minSize the minimum number of integers to generate (inclusive)
   * @param maxSize the maximum number of integers to generate (exclusive)
   * @param maxValue the upper bound on values (exclusive)
   * @param rng the random number generator to use
   * @return a vector of randomly drawn integers
   */
  private int[] getRandomInts(final int minSize, final int maxSize,
                              final int maxValue, final Random rng) {
    // Randomly choose a size for the result.
    int size = minSize + rng.nextInt(maxSize - minSize);
    // Select the correct number of integers and return them as a vector.
    return rng.ints(0, maxValue).limit(size).toArray();
  }

  /**
   * Converts a formatted string containing the average (in microseconds)
   * of a vector of times in nanoseconds.
   * @param times the nanosecond time observations
   * @return the average time in microseconds
   */
  private String asMicroSeconds(final long[] times) {
    return String.format("%.2f", Arrays.stream(times)
                                       .summaryStatistics()
                                       .getAverage() * NANOTOMICRO);
  }
}
