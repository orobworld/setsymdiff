# Symmetric Difference of Sets #

### What is this repository for? ###

The symmetric difference of two sets is the set consisting of those items that are elements of one of the original sets but not both. The purpose of this project is to test various ways for computing the size of the symmetric difference in Java.

### What does it do? ###

Pairs of sets are randomly generated and the size of their symmetric differences are computed using each proposed method. The tests are done on sets of integers, and may not generalize to sets of other types (in particular, those not implementing the `Comparable` interface). Accuracy (all four methods arriving at the same result for a particular pair of sets) is checked, and statistics are displayed for the average computation time per pair of sets. Tests are performed on instances of both `HashSet` and `TreeSet`.

Three of the six methods tested use only standard Java classes; the other three require the open source [Apache Commons Collections](https://commons.apache.org/proper/commons-collections/) library. (This code was developed with version 4.4 of that library.) The methods tested are as follows:

- using the `Streams` interface;
- using the `CollectionUtils.subtract()` method from the Apache library;
- using the `CollectionUtils.disjunction()` method;
- using the characteristic vector of the set difference, computed via streams and an instance of `BitSet`;
- using the intersection, computed via `Streams`; and
- using the intersection, computed via `CollectionUtils.intersection()`.

### How do I control it? ###

Up to five command line arguments can be given. Default values are used when fewer arguments are specified, and the program can be run with no arguments. Command line arguments and their defaults are as follows:

- the number of replications to perform (default 10,000);
- the upper limit on the integers appearing in a set (default 100,000; lower limit is always 0);
- the (approximate) minimum size of a set (default 100; some violations are tolerated);
- the maximum size of a set (default 200; hard constraint); and
- the seed for the random number generator.

Sadly, the output is unavoidably stochastic. Results will change even when the program is rerun with identical arguments, including the seed.

A discussion of why I did this, and a few results, can be found in a [blog post](https://orinanobworld.blogspot.com/2021/05/symmetric-difference-of-sets-in-java.html).

### License ###

The code here is copyrighted by Paul A. Rubin (yours truly) and licensed under a [Creative Commons Attribution 3.0 Unported License](http://creativecommons.org/licenses/by/3.0/deed.en_US). 

